import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import Qt5Compat.GraphicalEffects

import "../js/config.js" as CF

BackgroundCCSL {
    ColumnLayout {
        anchors.centerIn: parent
        Item {
            width: 100
            height: 125
            Image {
                id: logo
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                source: CF.logoImage
                sourceSize.width: parent.width
            }
            ColorOverlay {
                anchors.fill: logo
                source: logo
                color: "#FFF"
            }
        }
        Label {
            id: label
            Layout.alignment: Qt.AlignHCenter
            color: 'white'
            text: CF.trademark
            Component.onCompleted: label.font.pointSize += 6
        }
    }
}
