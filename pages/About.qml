import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Controls.Material 2.15
import QtQuick.Layouts 1.12
import Qt5Compat.GraphicalEffects

import '../components'
import '../js/config.js' as CF

PageCCSL {
    Flickable {
        id: flickable
        height: parent.height - tabBar.height - 10
        width: parent.width
        contentHeight: column.height
        maximumFlickVelocity: 5000
        boundsBehavior: Flickable.OvershootBounds
        Column {
            id: column
            width: parent.width
            spacing: 20
            Item {
                height: 100
                width: parent.width
                BackgroundCCSL {
                    id: backgroundPanel
                    anchors.fill: parent

                    RowLayout {
                        anchors.centerIn: parent
                        spacing: 10
                        Image {
                            id: logo
                            height: parent.height - 10
                            fillMode: Image.PreserveAspectFit
                            source: CF.logoImage
                            sourceSize.width: 59
                            sourceSize.height: 69
                            smooth: true
                        }
                        ColorOverlay {
                            anchors.fill: logo
                            source: logo
                            color: "#FFF"
                        }
                        Label {
                            color: 'white'
                            text: CF.trademark
                            font.pixelSize: parent.height / 3
                            anchors.verticalCenter: parent.verticalCenter
                        }
                    }
                }
            }
            StackLayout {
                id: stackLayout
                currentIndex: tabBar.currentIndex
                height: project.visible ? project.height : software.height
                width: parent.width
                Column {
                    id: project
                    width: parent.width
                    spacing: 20
                    PanelCCSL {
                        headerText: qsTr("Project")
                        width: parent.width
                        Label {
                            anchors.horizontalCenter: parent.horizontalCenter
                            width: parent.width * 0.7
                            text: qsTr(
                                      "<br>" + CF.trademark + "<br>A 'skeleton' to QML mobile applications"
                                      + "<br><br><b>Version: </b>" + Qt.application.version)
                            horizontalAlignment: Label.AlignHCenter
                            wrapMode: Label.Wrap
                            onLinkActivated: customTabs.openCustomTab(link)
                            linkColor: Material.secondaryTextColor
                        }
                        Column {
                            width: parent.width
                            Label {
                                x: parent.width / 2 - width / 2
                                text: "<br><b>Contributing and Licensing</b>"
                                horizontalAlignment: Label.AlignHCenter
                            }
                            Row {
                                anchors.horizontalCenter: parent.horizontalCenter
                                ToolButton {
                                    icon {
                                        source: "qrc:/assets/icons/gitlab.svg"
                                        height: 30
                                        width: 30
                                        color: Material.accent
                                    }
                                    background: Rectangle {
                                        opacity: (parent.pressed) ? 0.15 : 0
                                    }
                                    padding: 16
                                    onClicked: customTabs.openCustomTab(
                                                   "https://gitlab.com/ccsl-ufpa/ufpa-digital")
                                }
                                ToolButton {
                                    icon {
                                        source: "qrc:/assets/icons/balance.svg"
                                        height: 30
                                        width: 30
                                        color: Material.accent
                                    }
                                    background: Rectangle {
                                        opacity: (parent.pressed) ? 0.15 : 0
                                    }
                                    padding: 16
                                    onClicked: customTabs.openCustomTab(
                                                   "https://www.gnu.org/licenses/gpl-3.0.txt")
                                }
                            }
                        }
                    }
                    PanelCCSL {
                        headerText: qsTr("Team")
                        width: parent.width

                        Image {
                            id: ccslLogo
                            source: "qrc:/assets/images/logo-ccsl.svg"
                            fillMode: Image.PreserveAspectFit
                            sourceSize.width: 79
                            sourceSize.height: 100
                            smooth: true
                            x: parent.width / 2 - width / 2
                            horizontalAlignment: Image.AlignHCenter
                        }

                        Column {
                            width: parent.width
                            Label {
                                x: parent.width / 2 - width / 2
                                text: "<br>" + CF.trademark + " is a project by <b>Centro de Competência em Software Livre da UFPA</b>"
                                width: parent.width * 0.7
                                horizontalAlignment: Label.AlignHCenter
                                wrapMode: Label.Wrap
                                onLinkActivated: customTabs.openCustomTab(link)
                                linkColor: Material.secondaryTextColor
                            }
                            Row {
                                anchors.horizontalCenter: parent.horizontalCenter
                                ToolButton {
                                    icon {
                                        source: "qrc:/assets/icons/site.svg"
                                        height: 30
                                        width: 30
                                        color: Material.accent
                                    }
                                    background: Rectangle {
                                        opacity: (parent.pressed) ? 0.15 : 0
                                    }
                                    padding: 16
                                    onClicked: customTabs.openCustomTab(
                                                   "http://ccsl.ufpa.br")
                                }
                                ToolButton {
                                    icon {
                                        source: "qrc:/assets/icons/gitlab.svg"
                                        height: 30
                                        width: 30
                                        color: Material.accent
                                    }
                                    background: Rectangle {
                                        opacity: (parent.pressed) ? 0.15 : 0
                                    }
                                    padding: 16
                                    onClicked: customTabs.openCustomTab(
                                                   "https://gitlab.com/ccsl-ufpa")
                                }
                            }
                        }
                        Repeater {
                            model: ListModel {
                                ListElement {
                                    title: qsTr("Developer")
                                    name: qsTr("Lucas Gabriel de Souza<br>@souzaluuk")
                                    contacts: [
                                        ListElement {
                                            url: "https://twitter.com/souzaluuk"
                                            sourceIcon: "qrc:/assets/icons/twitter.svg"
                                        },
                                        ListElement {
                                            url: "https://gitlab.com/souzaluuk"
                                            sourceIcon: "qrc:/assets/icons/gitlab.svg"
                                        },
                                        ListElement {
                                            url: "https://github.com/souzaluuk/"
                                            sourceIcon: "qrc:/assets/icons/github.svg"
                                        },
                                        ListElement {
                                            url: "https://t.me/souzaluuk"
                                            sourceIcon: "qrc:/assets/icons/telegram.svg"
                                        },
                                        ListElement {
                                            url: "mailto:lucassouzaufpa@gmail.com"
                                            sourceIcon: "qrc:/assets/icons/email.svg"
                                        }
                                    ]
                                }
                                ListElement {
                                    title: qsTr("Supervisor")
                                    name: qsTr("Filipe Saraiva")
                                    contacts: [
                                        ListElement {
                                            url: "http://filipesaraiva.info"
                                            sourceIcon: "qrc:/assets/icons/site.svg"
                                        },
                                        ListElement {
                                            url: "https://gitlab.com/filipesaraiva"
                                            sourceIcon: "qrc:/assets/icons/gitlab.svg"
                                        },
                                        ListElement {
                                            url: "mailto:saraiva@ufpa.br"
                                            sourceIcon: "qrc:/assets/icons/email.svg"
                                        }
                                    ]
                                }
                            }
                            delegate: Column {
                                width: parent.width
                                Label {
                                    x: parent.width / 2 - width / 2
                                    text: "<b>" + name + "</b><br>" + title
                                    horizontalAlignment: Label.AlignHCenter
                                }
                                Row {
                                    anchors.horizontalCenter: parent.horizontalCenter
                                    Repeater {
                                        id: repeaterSocial
                                        model: contacts
                                        delegate: ToolButton {
                                            icon {
                                                source: sourceIcon
                                                height: 30
                                                width: 30
                                                color: Material.accent
                                            }
                                            background: Rectangle {
                                                opacity: (parent.pressed) ? 0.15 : 0
                                            }
                                            padding: 16
                                            onClicked: customTabs.openCustomTab(
                                                           url)
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                Column {
                    id: software
                    width: parent.width
                    spacing: 20
                    PanelCCSL {
                        headerText: qsTr("Libraries")
                        width: parent.width
                        Label {
                            anchors.horizontalCenter: parent.horizontalCenter
                            width: parent.width * 0.7
                            text: qsTr(
                                      "<br><b>Qt/QML</b>")
                            horizontalAlignment: Label.AlignHCenter
                            wrapMode: Label.Wrap
                            onLinkActivated: customTabs.openCustomTab(link)
                            linkColor: Material.secondaryTextColor
                        }
                        Row {
                            anchors.horizontalCenter: parent.horizontalCenter
                            ToolButton {
                                icon {
                                    source: "qrc:/assets/icons/site.svg"
                                    height: 30
                                    width: 30
                                    color: Material.accent
                                }
                                background: Rectangle {
                                    opacity: (parent.pressed) ? 0.15 : 0
                                }
                                padding: 16
                                onClicked: customTabs.openCustomTab(
                                               "https://qt.io")
                            }
                            ToolButton {
                                icon {
                                    source: "qrc:/assets/icons/balance.svg"
                                    height: 30
                                    width: 30
                                    color: Material.accent
                                }
                                background: Rectangle {
                                    opacity: (parent.pressed) ? 0.15 : 0
                                }
                                padding: 16
                                onClicked: customTabs.openCustomTab(
                                               "https://www.gnu.org/licenses/gpl-3.0.txt")
                            }
                        }
                        Label {
                            anchors.horizontalCenter: parent.horizontalCenter
                            width: parent.width * 0.7
                            text: qsTr("<br><b>Font Awesome</b>")
                            horizontalAlignment: Label.AlignHCenter
                            wrapMode: Label.Wrap
                            onLinkActivated: customTabs.openCustomTab(link)
                            linkColor: Material.secondaryTextColor
                        }
                        Row {
                            anchors.horizontalCenter: parent.horizontalCenter
                            ToolButton {
                                icon {
                                    source: "qrc:/assets/icons/site.svg"
                                    height: 30
                                    width: 30
                                    color: Material.accent
                                }
                                background: Rectangle {
                                    opacity: (parent.pressed) ? 0.15 : 0
                                }
                                padding: 16
                                onClicked: customTabs.openCustomTab(
                                               "https://fontawesome.com/")
                            }
                            ToolButton {
                                icon {
                                    source: "qrc:/assets/icons/balance.svg"
                                    height: 30
                                    width: 30
                                    color: Material.accent
                                }
                                background: Rectangle {
                                    opacity: (parent.pressed) ? 0.15 : 0
                                }
                                padding: 16
                                onClicked: customTabs.openCustomTab(
                                               "https://creativecommons.org/licenses/by/4.0/")
                            }
                        }
                        Label {
                            anchors.horizontalCenter: parent.horizontalCenter
                            width: parent.width * 0.7
                            text: qsTr("<br><b>Material Design</b>")
                            horizontalAlignment: Label.AlignHCenter
                            wrapMode: Label.Wrap
                            onLinkActivated: customTabs.openCustomTab(link)
                            linkColor: Material.secondaryTextColor
                        }
                        Row {
                            anchors.horizontalCenter: parent.horizontalCenter
                            ToolButton {
                                icon {
                                    source: "qrc:/assets/icons/site.svg"
                                    height: 30
                                    width: 30
                                    color: Material.accent
                                }
                                background: Rectangle {
                                    opacity: (parent.pressed) ? 0.15 : 0
                                }
                                padding: 16
                                onClicked: customTabs.openCustomTab(
                                               "https://material.io/")
                            }
                        }
                    }
                }
            }
        }
        ScrollIndicator.vertical: ScrollIndicator {}
    }
    TabBar {
        id: tabBar
        position: TabBar.Footer
        anchors.bottom: parent.bottom
        width: parent.width
        Material.elevation: 6
        Repeater {
            model: ListModel {
                ListElement {
                    title: qsTr("About")
                }
                ListElement {
                    title: qsTr("Libraries")
                }
            }
            delegate: TabButton {
                id: control
                text: title
                font.bold: true
            }
        }
    }
}
