import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Controls.Material 2.15
import QtQuick.Layouts 1.12
import QtLocation 5.6
import QtPositioning 5.6

import QtQuick.LocalStorage 2.12

import '../components'
import "../js/database.js" as DB

PageCCSL {
    PanelCCSL {
        headerText: qsTr('First section')
        width: parent.width
    }
    Plugin {
        id: mapPlugin
        name: "osm"
    }

    Map {
        id: map
        anchors.fill: parent
        plugin: mapPlugin
        center: QtPositioning.coordinate(-1.3461, -48.3829)
        zoomLevel: 14

        MapItemView {
            model: ListModel {
                id: locationModel
                Component.onCompleted: DB.showMarker()
            }
            delegate: MapQuickItem {
                id: marker
                sourceItem: Image {
                    id: img
                    source: "http://maps.gstatic.com/mapfiles/ridefinder-images/mm_20_red.png"
                }
                coordinate: QtPositioning.coordinate(latitude, longitude)
                anchorPoint.x: img.width / 2
                anchorPoint.y: img.height / 2
            }
        }

        MouseArea {
            id: mouseArea
            anchors.fill: map

            onDoubleClicked: {
                if (mouse.button === Qt.LeftButton){
                    var latitude = map.toCoordinate(Qt.point(mouse.x,mouse.y)).latitude
                    var longitude = map.toCoordinate(Qt.point(mouse.x,mouse.y)).longitude
                    stack.push(registration_page,{"latitude":latitude,"longitude":longitude})
               }
            }
        }
    }
}
